import time

import requests
import calendar
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString

from globalVarAndMethod import *


def render_page_without_big_photo(div_tag: Tag, page_url: str):
    central_tag: Tag = div_tag.contents[0].contents[1]

    main_tag: Tag = central_tag.contents[0].contents[0].contents[0]
    time_tag: Tag = central_tag.contents[0].contents[0].contents[1]
    comment_tag_list: list = central_tag.contents[1].contents[0].contents[1].contents
    # comment_tag: Tag = central_tag.contents[0].contents[0].contents[2] # seems wrong

    name_tag: Tag = main_tag.contents[0]
    article_tag: Tag = main_tag.contents[1]
    # element in index 2 may be quote or photo, photo in mobile page will be compressed
    if len(main_tag.contents) >= 3:
        third_tag: Tag = main_tag.contents[2]

    anchor_tag: Tag = name_tag.contents[0].contents[0].contents[0].contents[1].contents[0].contents[0].contents[0] \
        .contents[0].contents[0]
    poster_name = str(anchor_tag.contents[0])
    publish_time = str(time_tag.contents[0].contents[0].contents[0])
    localtime = time.localtime(time.time())
    year = localtime.tm_year
    month = localtime.tm_mon
    day = localtime.tm_mday
    if publish_time[:2] == "昨天":
        day -= 1
        if day == 0:
            month -= 1
            if month == 2 and calendar.isleap(year):
                day = 29
            if month == 2 and not calendar.isleap(year):
                day = 28
            if month == 0:
                year -= 1
                month = 12
                day = 31

        publish_time = str(month) + "/" + str(day) + publish_time[2:]
    if YEAR_CHAR not in publish_time:
        publish_time = str(year) + YEAR_CHAR + publish_time
    poster_name = poster_name + HTML_LINE_BREAK + SINGLE_LINE_BREAK
    publish_time = publish_time + HTML_LINE_BREAK + SINGLE_LINE_BREAK
    page_url = page_url.replace("m.", "www.", 1)
    page_url = get_str_wrapped_with_anchor_tag(page_url) + HTML_LINE_BREAK + SINGLE_LINE_BREAK

    content_list = article_tag.contents[0].contents
    # some posts contents will be located in deeper level
    if len(content_list) == 1 and len(content_list[0].contents) >= 2:
        content_list = content_list[0].contents
        count_br = 0
        for i in range(len(content_list)):
            element = content_list[i]
            if type(element) == Tag:
                if element.name == "span" and len(element.contents) > 0:
                    if type(element.contents[0]) == NavigableString:
                        stripped_str = element.contents[0].strip()
                        element.contents[0] = NavigableString(stripped_str)
                if element.name == "br":
                    count_br += 1
                    if count_br % 2 == 0:
                        content_list[i] = EMPTY_SEPARATOR  # I dont want too many line breaks
    else:
        # FB splits sentence and url to several pieces... to get the unshortened url or use the url, do this loop:
        for i in range(len(content_list)):
            for j in range(len(content_list[i].contents)):
                if content_list[i].contents[j].name == "a":
                    content_list[i].contents[j].attrs = {}  # clear the anchor href
                    for k in range(len(content_list[i].contents[j].contents)):
                        is_url_eliminating_active = False

                        contents_k = content_list[i].contents[j].contents[k]
                        new_str = str(contents_k)
                        new_str = new_str.replace(START_SPAN, EMPTY_SEPARATOR)
                        new_str = new_str.replace(START_SPAN_WITH_WORD_BREAK_CLASS, EMPTY_SEPARATOR)
                        if "<span" not in new_str:
                            new_str = new_str.replace(END_SPAN, EMPTY_SEPARATOR)
                            is_url_eliminating_active = True
                        new_str = new_str.replace(WBR, EMPTY_SEPARATOR)

                        if is_url_eliminating_active:
                            new_str = ELIMINATE_REDUNDANT_URL_PARAM_SUFFIX(new_str)

                        # use this if to prevent html render unexpected text e.g. <span>#</span>abc instead of #abc
                        if "<span" in new_str:
                            if type(contents_k) == Tag:
                                if len(contents_k.contents) == 1:
                                    text = str(contents_k.contents[0])
                                    content_list[i].contents[j].contents[k] = NavigableString(text)
                        else:
                            content_list[i].contents[j].contents[k] = NavigableString(new_str)
        print()
    content_list = [str(x).strip() for x in content_list]

    content_list = [poster_name] + [page_url] + [publish_time] + content_list
    return content_list


# todo: undone method
def render_page_with_big_photo(div_tag: Tag, page_url):
    main_tag2: Tag = div_tag.contents[0].contents[0].contents[0].contents[0].contents[3].contents[0] \
        .contents[0]
    time_tag2: Tag = div_tag.contents[0].contents[0].contents[0].contents[0].contents[3].contents[0] \
        .contents[1]
    name_tag2: Tag = main_tag2.contents[0]
    article_tag2: Tag = main_tag2.contents[2]
    strong_tag: Tag = name_tag2.contents[0]
    poster_name2 = str(strong_tag.contents[0])
    print("last")


def get_str_wrapped_with_p_tag(str_in_method):
    return START_P + str_in_method + END_P


def get_str_wrapped_with_anchor_tag(str_in_method):
    return START_ANCHOR + str_in_method + END_ANCHOR


def get_objects_container_div_tag(page_url):
    res = requests.get(page_url)
    soup_in_method = BeautifulSoup(res.text, features="lxml")
    html_tag: Tag = soup_in_method.contents[2]
    body_tag: Tag = html_tag.contents[1]
    first_div_tag: Tag = body_tag.contents[0]
    viewport_div_tag: Tag = first_div_tag.contents[0]
    objects_container_div_tag: Tag = viewport_div_tag.contents[1]
    return objects_container_div_tag


pageUrl = "https://m.facebook.com/PttTW/posts/10157743956136364"
if pageUrl[8:11] == "www":
    pageUrl = pageUrl.replace("www", "m", 1)

pageUrlWithPhotoBelow = "https://m.facebook.com/PttTW/posts/10157743956136364"
pageWithLink = "https://m.facebook.com/PttTW/posts/10157749263851364"
pageUrlWithBigPhoto = "https://m.facebook.com/init.kobeengineer/photos/a.1416496745064002/4075091239204526/"

objectsContainerDivTag: Tag = get_objects_container_div_tag(pageUrl)

finalList = []
# todo: condition should change: bcs in desktop pages, url and layout all look the same, but in mobile page,
# urls are the same and photo positions are different(could be above or below),
# maybe should decide from div id=MPhotoContent(photo above) and
# id=m_story_permalink_view(photo below and post with external link?)
if FB_DOMAIN_WITH_PHOTO_BELOW in pageUrl or URL_WITH_STORY_FB_ID_PARAM in pageUrl:
    finalList = render_page_without_big_photo(objectsContainerDivTag, pageUrl)
elif FB_DOMAIN_WITH_BIG_PHOTO in pageUrl:
    finalList = render_page_with_big_photo(objectsContainerDivTag, pageUrl)

if len(finalList) != 0:
    startHtml = START_HTML.replace("title puts here", "FB_Post")
    finalList = [startHtml] + finalList + [END_HTML]
    finalStr = EMPTY_SEPARATOR.join(finalList)
    # finalStr = SINGLE_LINE_BREAK.join(finalList)  # for test-readability purpose
    finalStr = finalStr.replace(MOBILE_URL_PREFIX, DESKTOP_URL_PREFIX)
    finalStr = finalStr.replace(WBR, EMPTY_SEPARATOR)
    finalStr = finalStr.replace(HTML_END_LINE_BREAK, HTML_END_LINE_BREAK + HTML_LINE_BREAK)
    finalStr = finalStr + END_STR

    with open(
            "facebookPost.html",
            "w",
            encoding="utf-8"
    ) as text_file:
        print(finalStr, file=text_file)
else:
    print("nothing happens")
