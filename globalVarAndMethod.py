COLOR_PREFIX = "color:"

PTT_RED_LIGHT = "f1 hl"
PTT_RED_DARK = "f1"
PTT_PINK_LIGHT = "f5 hl"
PTT_PINK_DARK = "f5"
PTT_YELLOW_LIGHT = "f3 hl"
PTT_YELLOW_DARK = "f3"
PTT_GREEN_LIGHT = "f2 hl"
PTT_GREEN_DARK = "f2"
PTT_TEAL_LIGHT = "f6 hl"
PTT_TEAL_DARK = "f6"
PTT_BLUE_LIGHT = "f4 hl"
PTT_BLUE_DARK = "f4"

LIGHT_RED = "rgb(252, 18, 51)"
LIGHT_PINK = "rgb(255, 102, 255)"
LIGHT_YELLOW = "rgb(229, 158, 37)"
LIGHT_GREEN = "rgb(24, 168, 65)"
# DARK_GREEN = "rgb(112, 244, 112)"
LIGHT_TEAL = "rgb(26, 169, 178)"
# DARK_TEAL = "rgb(33, 247, 247)"
BLUE = "rgb(24, 133, 226)"

COLOR_DICT = {
    PTT_RED_LIGHT: COLOR_PREFIX + LIGHT_RED,
    PTT_RED_DARK: COLOR_PREFIX + LIGHT_RED,
    PTT_PINK_LIGHT: COLOR_PREFIX + LIGHT_PINK,
    PTT_PINK_DARK: COLOR_PREFIX + LIGHT_PINK,
    PTT_YELLOW_LIGHT: COLOR_PREFIX + LIGHT_YELLOW,
    PTT_YELLOW_DARK: COLOR_PREFIX + LIGHT_YELLOW,
    PTT_GREEN_LIGHT: COLOR_PREFIX + LIGHT_GREEN,
    PTT_GREEN_DARK: COLOR_PREFIX + LIGHT_GREEN,
    PTT_TEAL_LIGHT: COLOR_PREFIX + LIGHT_TEAL,
    PTT_TEAL_DARK: COLOR_PREFIX + LIGHT_TEAL,
    PTT_BLUE_LIGHT: COLOR_PREFIX + BLUE,
    PTT_BLUE_DARK: COLOR_PREFIX + BLUE,
}
REURL_CC = "reurl.cc"
SHORTENED_URL_NAME_DICT = {
    "reurlCC": REURL_CC,
    "bitLy": "bit.ly",
    "bityl.co": "bityl.co",
    "tinyurl": "tinyurl",
    "0rz.tw": "0rz.tw",
    "t.co": "t.co",
    "pse.is": "pse.is",
    "ssur.cc": "ssur.cc",
    "is.gd": "is.gd",
    "yns.page.link": "yns.page.link",
    "lat.ms": "lat.ms",  # LA Times
    "goo.gl": "goo.gl",
    "tinyl": "tinyl",
    "rebrand.ly": "rebrand.ly",
    "myppt.cc": "myppt.cc",  # still not work, need to solve captcha
    "ppt.cc": "ppt.cc",
}
# ?story_fbid= is required
REDUNDANT_URL_SUFFIX_DICT = {
    "from=": "from=",
    "utm_medium=": "utm_medium=",
    "utm_source=": "utm_source=",
    "utm_campaign=": "utm_campaign=",
    "utm_content=": "utm_content=",
    "utm_term=": "utm_term=",
    "fbclid=": "fbclid=",
    "__cft__[0]=": "__cft__[0]=",
    "__tn__=": "__tn__=",
    "cid=": "cid=",  # dcard
    "ref=": "ref=",  # dcard e.g. ref=ios
    "chdtv": "chdtv",
    "type=": "type=",
    "ctrack=": "ctrack=",
    "mibextid=": "mibextid=",
}

PTT_INDEX_PAGE = "/bbs/Gossiping/index.html"
PTT_ROOT_URL = "https://www.ptt.cc"
PTT_POST_404_NOT_FOUND = "404 - Not Found."
PTT_18_CONFIRMATION_PAGE = PTT_ROOT_URL + "/ask/over18"
PTT_18_CONFIRMATION_PAGE_WITH_FROM_PARAM = PTT_18_CONFIRMATION_PAGE + "?from=/bbs/Gossiping/index.html"
HTTP_PREFIX = "http://"
HTTPS_PREFIX = "https://"
IMGUR_HOST_NAME = "imgur.com"
IMGUR_PREFIX = "https://imgur"
IMGUR_PREFIX_WITHOUT_S = "http://imgur"
IMGUR_PREFIX_A = "https://imgur.com/a/"
EVERNOTE_PREFIX = "https://www.evernote"
DESKTOP_URL_PREFIX = "https://www."
MOBILE_URL_PREFIX = "https://m."
MP4_SUFFIX = ".mp4"
FB_MOBILE_DOMAIN = "m.facebook"
FB_DESKTOP_DOMAIN = "www.facebook"
FB_DOMAIN_WITH_PHOTO_BELOW = "posts"
URL_WITH_STORY_FB_ID_PARAM = "story.php?story_fbid="
FB_DOMAIN_WITH_BIG_PHOTO = "photos"
YT_HTTPS_PREFIX = "https://youtu"
YT_HTTPS_PREFIX_WITH_WWW = "https://www.youtu"
YT_DOMAIN = "www.youtube"
YT_MOBILE_DOMAIN = "https://m.youtube"
YT_SHORTENED_DOMAIN = "youtu.be"
JPG_SUFFIX = ".jpg"
JPEG_SUFFIX = ".jpeg"
PNG_SUFFIX = ".png"
GIF_SUFFIX = ".gif"
MD_IMG_SYNTAX_PREFIX = "![]("
CLOSE_PARENTHESIS = ")"
EMPTY_SEPARATOR = ""
SPACE_SEPARATOR = " "
FOUR_SPACE = "    "
THREE_TILDE = "~~~"
HYPHEN_OR_DASH = "-"
QUESTION_MARK = "?"
AMPERSAND = "&"
FOUR_SPACE_REPLACEMENT = "~++~"
THREE_TILDE_REPLACEMENT = "~+~"
END_STR = "＃"
WINDOWS_LINE_BREAK = "\r\n"
SINGLE_LINE_BREAK = "\n"
DOUBLE_LINE_BREAK = "\n\n"
HTML_LINE_BREAK = "<br>"
HTML_END_LINE_BREAK = "<br/>"
WBR = "<wbr/>"
START_ANCHOR = "<a>"
END_ANCHOR = "</a>"
START_P = "<p>"
END_P = "</p>"
START_SPAN = "<span>"
END_SPAN = "</span>"
START_SPAN_WITH_WORD_BREAK_CLASS = "<span class=\"word_break\">"
START_DIV = "<div>"
END_DIV = "</div>"
YEAR_CHAR = "年"
pttArticleEndText = "\n--\n"

START_HTML = '''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title puts here</title>
</head>
<body>
'''
END_HTML = '''
</body>
</html>
'''

UNEXPECTED_TEXT_LIST = ["1;31m", "1;33m", "[37m", "備註請放最後面 違者新聞文章刪除"]

PTT_GOSSIPING_POLICY_TEXT = [
    "※註:有電視或媒體有報導者，請勿使用爆卦!，切勿用爆卦張貼新聞",
    "無重大八卦請勿使用此分類，否則視同濫用爆卦鬧板(文章退回、水桶6個月)",
    "未滿30繁體中文字",
    "水桶3個月",
    "備註請放最後面 違者新聞文章刪除",
    "※ 例如蘋果日報、自由時報（請參考版規下方的核准媒體名單）",
    "※ 若新聞沒有記者名字或編輯名字，請勿張貼，否則會被水桶14天",
    "※ 外電至少要有來源或編輯 如:法新社",
    "※ 標題沒有完整寫出來 ---> 依照板規刪除文章",
    "※ 社論特稿都不能貼！違者刪除（政治類水桶3個月)，貼廣告也會被刪除喔！可詳看版規",
    "※ 當新聞連結過長時，需提供短網址方便網友點擊",
    "※ 一個人三天只能張貼一則新聞，被刪或自刪也算額度內，超貼者水桶，請注意",
    "※ 一個人一天只能張貼一則新聞(以天為單位)，被刪或自刪也算額度內，超貼者水桶，請?",
    "※ 一個人一天只能張貼一則新聞(以天為單位)，被刪或自刪也算額度內，超貼者水桶，請注意",
    "※ 備註請勿張貼三日內新聞(包含連結、標題等)",
    "※ Yahoo、MSN、LINE等非直接官方新聞連結不被允許",
    "※「新聞」標題須為原新聞標題且從頭張貼 ※",
    "※ 請附上有效原文連結或短網址 ※",
    "※ 內文請完整轉載標題 請勿修改與刪減 ※",
    "※ 注意發文標題 為原始新聞標題從頭張貼 切勿修改與刪減 ※",
    "※ 請完整轉載原文 請勿修改內文與刪減 ※",
    "※ 40字心得、備註 ※",
    "※ 「Live」、「新聞」、「轉錄」、「舊聞」及 轉錄他方內容之文章",
    "每日發文數總上限為3篇，自刪與板主刪除，同樣計入額度 ※",
]

GOSSIPING_FORMAT_TEXT_LIST = [
    "1.媒體來源:",
    "2.記者署名:",
    "3.完整新聞標題:",
    "4.完整新聞內文:",
    "完整新聞內文:",
    "5.完整新聞連結 (或短網址):",
]


def ELIMINATE_REDUNDANT_URL_PARAM_SUFFIX(url_in_method: str):
    for redundantElement in REDUNDANT_URL_SUFFIX_DICT:
        # evernote put fbClId after question mark and chain other important params behind fbClId...
        if EVERNOTE_PREFIX in url_in_method:
            print("evernote with ?fb")
            break
        if QUESTION_MARK + redundantElement in url_in_method:
            temp_element = QUESTION_MARK + redundantElement
            suffix_index = url_in_method.index(temp_element)
            redundant_key_and_equal_len = len(temp_element)
            try:
                next_ampersand_index = url_in_method.index("&", suffix_index + redundant_key_and_equal_len)
            except:
                next_ampersand_index = "none"
            if type(next_ampersand_index) == int:
                # means that there are other suffix with &, I have to replace the next first one "&" with "?"
                url_in_method = url_in_method.replace(url_in_method[suffix_index:next_ampersand_index + 1], "?")
            else:
                url_in_method = url_in_method[:suffix_index]
                break
        elif AMPERSAND + redundantElement in url_in_method:
            temp_element = AMPERSAND + redundantElement
            suffix_index = url_in_method.index(temp_element)
            redundant_key_and_equal_len = len(temp_element)
            try:
                next_ampersand_index = url_in_method.index("&", suffix_index + redundant_key_and_equal_len)
            except:
                next_ampersand_index = "none"
            if type(next_ampersand_index) == int:
                url_in_method = url_in_method.replace(url_in_method[suffix_index:next_ampersand_index], "")
            else:
                url_in_method = url_in_method[:suffix_index]

    print("url after eliminating redundant suffix:", url_in_method)
    return url_in_method


PTT_GROWN_UP_WARNING = '''



批踢踢實業坊









本網站已依網站內容分級規定處理
警告︰您即將進入之看板內容需滿十八歲方可瀏覽。
若您尚未年滿十八歲，請點選離開。若您已滿十八歲，亦不可將本區之內容派發、傳閱、出售、出租、交給或借予年齡未滿18歲的人士瀏覽，或將本網站內容向該人士出示、播放或放映。






我同意，我已年滿十八歲進入


未滿十八歲或不同意本條款離開








'''
