import sys
import warnings
from typing import Union, Dict
from urllib.parse import unquote

import requests
from bs4 import BeautifulSoup
from bs4.element import NavigableString, Tag
from requests import Response

from globalVarAndMethod import *

warnings.filterwarnings("ignore", ".*looks like a URL.*", category=UserWarning, module='bs4')


def print_hi(name):
    print(f'Hi, {name}')


def get_img_url_at_imgur_with_domain_a(imgur_url):
    res_in_method = requests.get(imgur_url)
    soup_in_method = BeautifulSoup(res_in_method.text, features="lxml")
    big_tag: Tag = soup_in_method.contents[1]
    head_tag: Tag = big_tag.contents[1]
    right_tag: Tag = head_tag.find("meta", attrs={"name": "twitter:image"})
    url: str = right_tag.attrs["content"]
    return url


# <br> can be normally rendered in markdown but not in Evernote, so use double \n
def get_str_after_replacing_single_line_break(
        str_to_be_handled: str, str_to_replace: str = DOUBLE_LINE_BREAK, should_replace_last_char: bool = True
) -> str:
    str_to_be_handled = str_to_be_handled.strip(SPACE_SEPARATOR)  # only trim white space, not include \n or \t etc.

    line_break_position = -1
    line_break_count = 0
    str_to_be_replaced = SINGLE_LINE_BREAK
    index_correct_count = 0
    for index_in_function in range(len(str_to_be_handled)):
        corrected_index = index_in_function + index_correct_count
        length_this_loop = len(str_to_be_handled)
        if str_to_be_handled[corrected_index] == str_to_be_replaced \
                or str_to_be_handled[corrected_index] == WINDOWS_LINE_BREAK:
            if corrected_index == length_this_loop - 1:  # if it's the last one, replace directly
                if should_replace_last_char:
                    str_to_be_handled = str_to_be_handled[:corrected_index] + str_to_replace
            if length_this_loop == 1:
                # delete single line break to prevent the line break from rendering as space when the str len is 1
                str_to_be_handled = str_to_replace
                index_correct_count += len(str_to_replace) - len(str_to_be_replaced)
                continue
            if line_break_count == 0:
                line_break_position = corrected_index  # record the first unpaired line break position
            line_break_count += 1
            if line_break_count == 2:  # paired line breaks can render normally, so do the reset
                line_break_count = 0
                line_break_position = -1
        if str_to_be_handled[corrected_index] != str_to_be_replaced and line_break_position != -1:
            if line_break_count == 1:
                # delete single line break to prevent the single line break from rendering as space
                str_to_be_handled = \
                    str_to_be_handled[:line_break_position] + \
                    str_to_replace + \
                    str_to_be_handled[line_break_position + 1:]
                index_correct_count += len(str_to_replace) - len(str_to_be_replaced)
            line_break_count = 0
            line_break_position = -1
    return str_to_be_handled


def get_url_wrapped_with_markdown(url: str, add_jpg_suffix=True) -> str:
    img_suffix = ""
    if add_jpg_suffix:
        img_suffix = JPG_SUFFIX

    return url + DOUBLE_LINE_BREAK + MD_IMG_SYNTAX_PREFIX + url + img_suffix + CLOSE_PARENTHESIS + DOUBLE_LINE_BREAK


def is_img_url_with_extension_at_end(url_in_method):
    return url_in_method[-4:] == JPG_SUFFIX or \
           url_in_method[-5:] == JPEG_SUFFIX or \
           url_in_method[-4:] == PNG_SUFFIX or \
           url_in_method[-4:] == GIF_SUFFIX


def get_redirected_url(extracted_url: str):
    for elementDomain in SHORTENED_URL_NAME_DICT.values():
        if elementDomain in extracted_url:
            try:
                res_for_shortened_url = requests.get(extracted_url)

                if elementDomain == REURL_CC:
                    extracted_url = res_for_shortened_url.headers["target"]
                else:
                    extracted_url = res_for_shortened_url.url

                res_in_function = get_confirmed_18_res(extracted_url)
                extracted_url = res_in_function.url
                extracted_url = unquote(extracted_url)
            except requests.ConnectionError as e:
                print("Umhh...", e)
                extracted_url = EMPTY_SEPARATOR
                break
    return extracted_url


def wrap_img_url_or_append_to_dict(
        extracted_url: str, is_wrapping_instant=True, url_dict=None, index_in=None
) -> Union[str, Dict[int, str]]:
    if url_dict is None:
        url_dict = {}
    if is_wrapping_instant:
        if extracted_url[:13] == IMGUR_PREFIX and not is_img_url_with_extension_at_end(extracted_url):
            if extracted_url[:20] == IMGUR_PREFIX_A:
                extracted_url = get_img_url_at_imgur_with_domain_a(extracted_url)
            extracted_url = get_url_wrapped_with_markdown(extracted_url)
        # suppose other img urls all have the img extension...
        elif is_img_url_with_extension_at_end(extracted_url):
            extracted_url = get_url_wrapped_with_markdown(extracted_url, False)
        return extracted_url
    else:
        url_dict[index_in] = extracted_url
        return url_dict


def get_yt_info_text(yt_url):
    res_in_method = requests.get(yt_url)
    soup_in_method = BeautifulSoup(res_in_method.text, features="lxml")

    title_tag = soup_in_method.find("meta", attrs={'name': 'title'})
    channel_tag = soup_in_method.find("link", attrs={'itemprop': 'name'})
    date_published_tag = soup_in_method.find("meta", attrs={'itemprop': 'datePublished'})

    if title_tag is None or channel_tag is None or date_published_tag is None:
        text = yt_url
    else:
        title = title_tag.attrs["content"]
        channel = channel_tag.attrs["content"]
        date_published = date_published_tag.attrs["content"]
        text = title + DOUBLE_LINE_BREAK + yt_url + DOUBLE_LINE_BREAK + channel + DOUBLE_LINE_BREAK + date_published

    return text


def get_extracted_url(tag_in_method: Tag):
    if type(tag_in_method.contents[0]) == NavigableString:
        extracted_url: str = str(tag_in_method.contents[0])
    else:  # if it's not NavigableString, then is str
        extracted_url: str = tag_in_method.contents[0]
    extracted_url = get_redirected_url(extracted_url)
    if extracted_url != EMPTY_SEPARATOR:
        extracted_url = ELIMINATE_REDUNDANT_URL_PARAM_SUFFIX(extracted_url)
        if (YT_DOMAIN in extracted_url or YT_SHORTENED_DOMAIN in extracted_url) and "channel" not in extracted_url:
            extracted_url = get_yt_info_text(extracted_url)

    return extracted_url


def get_confirmed_18_res(ptt_url_or_shortened_url_but_is_actually_ptt_url):
    response_in_function = requests.get(ptt_url_or_shortened_url_but_is_actually_ptt_url)
    if PTT_18_CONFIRMATION_PAGE in response_in_function.url:
        session_obj = requests.Session()
        payload = {
            "from": PTT_INDEX_PAGE,
            "yes": "yes"
        }
        session_obj.post(PTT_18_CONFIRMATION_PAGE_WITH_FROM_PARAM, payload)
        response_in_function = session_obj.get(pageUrl)

    return response_in_function


def get_html_list(res_in_function: Response) -> list:
    soup = BeautifulSoup(res_in_function.text, features="lxml")
    content_and_comments = soup.select("div.bbs-screen.bbs-content")
    content_and_comments_html = content_and_comments[0]

    return content_and_comments_html.contents


def insert_info_and_get_list(list_in_function: list, html_list: list, page_url) -> list:
    list_in_function.insert(0, html_list[0])  # author
    list_in_function.insert(0, START_ANCHOR + page_url + END_ANCHOR)
    list_in_function.insert(0, html_list[2])  # title

    return list_in_function


def get_list_except_meta_tag(list_in_function: list) -> list:
    # Besides author, I only want meta value instead of meta tag(作者 看板 標題 時間)
    return list_in_function[1:]


def get_url_info_without_contents(url_in_function):
    return_str = url_in_function
    is_ptt_post404_not_found = False

    response_in_function = get_confirmed_18_res(pageUrl)
    html_list = get_html_list(response_in_function)

    for i_in_func in range(4):
        item_in_list = html_list[i_in_func]

        if type(item_in_list) == NavigableString:
            html_list[i_in_func] = str(item_in_list)
            string: str = html_list[i_in_func]

            if string == PTT_POST_404_NOT_FOUND:
                is_ptt_post404_not_found = True
                break

        if type(item_in_list) == Tag:
            tag_in_func = item_in_list
            tag_contents: list = tag_in_func.contents

            if tag_in_func.name == "div":
                if len(tag_contents[0].contents) > 0:
                    if str(tag_contents[0].contents[0]) == "作者":
                        space_navigable_str = NavigableString(SPACE_SEPARATOR)
                        html_list[i_in_func].contents.insert(1, space_navigable_str)
                    else:
                        html_list[i_in_func].contents = get_list_except_meta_tag(tag_contents)

        html_list[i_in_func] = str(html_list[i_in_func])

    if is_ptt_post404_not_found:
        return_str = return_str + "\n\n404..."
    else:
        final_list = [html_list[3]]
        final_list = insert_info_and_get_list(final_list, html_list, url_in_function)
        return_str = EMPTY_SEPARATOR.join(final_list)

    return return_str


def decode_cloud_flare_email(encoded_string):
    r = int(encoded_string[:2], 16)
    email = ''.join(
        [chr(int(encoded_string[i_in_func:i_in_func + 2], 16) ^ r) for i_in_func in range(2, len(encoded_string), 2)]
    )

    return email


if __name__ == '__main__':
    print_hi('PyCharm Pro')

pageUrl = "https://www.ptt.cc/bbs/gossiping/M.1621785416.A.199.html"
isImgRenderingEnabled = True
isLineBreakNormal = True
commentNumControl = 0

response = get_confirmed_18_res(pageUrl)
htmlList = get_html_list(response)
htmlLength = len(htmlList)
firstCommentIndex = 0
checkDivCount = 0
emptySpan = BeautifulSoup().new_tag("span")
anchorUrlDict: Dict[int, list] = {}
""" key is htmlList index \n
list index 0 is url, 1 is flag to wrap it 
"""
spanUrlDict: Dict[int, str] = {}
"""key is htmlList index"""
commentCount = 0
isPttPost404NotFound = False
for i in range(htmlLength):
    itemInList = htmlList[i]

    if type(itemInList) == NavigableString:
        htmlList[i] = str(itemInList)
        itemStr: str = htmlList[i]

        if itemStr == PTT_POST_404_NOT_FOUND:
            isPttPost404NotFound = True
            break

        if isLineBreakNormal:
            itemStr = itemStr.replace(DOUBLE_LINE_BREAK, DOUBLE_LINE_BREAK + HTML_LINE_BREAK + DOUBLE_LINE_BREAK)
        else:
            # if the article has too many unnecessary line breaks, use below
            itemStr = itemStr.replace(DOUBLE_LINE_BREAK, DOUBLE_LINE_BREAK + DOUBLE_LINE_BREAK)

        # four spaces would be rendered as code fence by markdown
        itemStr = itemStr.replace(FOUR_SPACE, FOUR_SPACE_REPLACEMENT)

        htmlList[i] = get_str_after_replacing_single_line_break(itemStr)

        itemStr = htmlList[i]
        hyphenPosition = -1
        hyphenCount = 0
        strToInsert = SINGLE_LINE_BREAK
        jCorrectCount = 0
        # noinspection PyUnboundLocalVariable
        for j in range(len(itemStr)):
            correctedJ = j + jCorrectCount
            if itemStr[correctedJ] == HYPHEN_OR_DASH:
                if hyphenCount == 0:
                    hyphenPosition = correctedJ
                hyphenCount += 1
            if itemStr[correctedJ] != HYPHEN_OR_DASH and hyphenPosition != -1:
                # if hyphenCount >= 3:
                # add line break before the hyphen to solve the 3 hyphens render issue
                # string = string[:hyphenPosition] + strToInsert + string[hyphenPosition:]
                # htmlList[i] = string
                # jCorrectCount += len(strToInsert)
                hyphenCount = 0
                hyphenPosition = -1

    if type(itemInList) == Tag:
        tag = itemInList
        tagContents: list = tag.contents
        shouldLineBreakAddAtTheEnd = False
        if len(tagContents) == 4:
            commentCount += 1
            if commentCount >= commentNumControl:
                firstCommentIndex = i
                break  # we wont address the comments section, so the loop ultimately ends here.
        if tag.name == "div":
            if checkDivCount < 4:  # author, title and post time
                if len(tagContents[0].contents) > 0:
                    if str(tagContents[0].contents[0]) == "作者":
                        spaceNavigableStr = NavigableString(SPACE_SEPARATOR)
                        htmlList[i].contents.insert(1, spaceNavigableStr)
                    else:
                        htmlList[i].contents = get_list_except_meta_tag(tagContents)
                checkDivCount += 1
            if tagContents[0].name == "img":
                # if is_img_url_with_extension_at_end(tagContents[0].attrs["src"]) and \
                #         IMGUR_PREFIX not in tagContents[0].attrs["src"]:
                #     anchorUrlDict[i - 2][1] = False  # i-2 is the corresponding list, i-1 is double line break
                if isImgRenderingEnabled:
                    if "imgur" in tagContents[0].attrs["src"]:
                        htmlList[i].contents[0] = NavigableString("")
                else:
                    htmlList[i].contents[0] = NavigableString("")
        if tag.name == "a":
            # 最後一行文章網址的URL有多包一層span 不要這個
            if type(tagContents[0]) == str or type(tagContents[0]) == NavigableString:
                tagContentStr = str(tagContents[0])
                if tagContentStr[0] == "#":
                    # make hash code's href from relative url to absolute url
                    tag.attrs["href"] = PTT_ROOT_URL + tag.attrs["href"]

                    href = tag.attrs["href"]
                    htmlList[i] = NavigableString(str(tag) + get_url_info_without_contents(href))
                elif "class" in tag.attrs and tag.attrs["class"][0] == '__cf_email__':
                    tagContentStr = decode_cloud_flare_email(tag.attrs["data-cfemail"])
                    htmlList[i] = NavigableString(tagContentStr)
                else:
                    # extract the plain URL
                    extractedURL = get_extracted_url(tag)
                    htmlList[i] = extractedURL
                    if extractedURL != EMPTY_SEPARATOR:
                        if PTT_ROOT_URL in extractedURL:
                            htmlList[i] = get_url_info_without_contents(extractedURL)
                        else:
                            anchorUrlDict[i] = [extractedURL, True]
        if tag.name == "span":
            # some url put in span with color and did not change the color in official page
            if tagContents[0].name == "a":
                anchorTag = tagContents[0]
                if type(anchorTag.contents[0]) == str or type(anchorTag.contents[0]) == NavigableString:
                    # extract the plain URL
                    extractedURL = get_extracted_url(anchorTag)
                    htmlList[i] = extractedURL
                    if extractedURL != EMPTY_SEPARATOR:
                        if PTT_ROOT_URL in extractedURL:
                            htmlList[i] = get_url_info_without_contents(extractedURL)
                        else:
                            anchorUrlDict[i] = [extractedURL, True]
            else:
                if len(tagContents) >= 2:
                    if type(tagContents[1]) == Tag:
                        if tagContents[1].name == "a":
                            urlInSpanAndA = tagContents[1].contents[0]
                            urlInSpanAndA = get_redirected_url(urlInSpanAndA)
                            if urlInSpanAndA != EMPTY_SEPARATOR:
                                urlInSpanAndA = ELIMINATE_REDUNDANT_URL_PARAM_SUFFIX(urlInSpanAndA)
                                if PTT_ROOT_URL in urlInSpanAndA:
                                    urlInSpanAndA = get_url_info_without_contents(urlInSpanAndA)
                                else:
                                    spanUrlDict = wrap_img_url_or_append_to_dict(urlInSpanAndA, False, spanUrlDict, i)
                            tagContents[1].contents[0] = NavigableString(urlInSpanAndA)
                contentStr = str(tagContents[0])
                if contentStr in PTT_GOSSIPING_POLICY_TEXT:
                    htmlList[i] = emptySpan
                # these two can not wrapped with html and body tags by BeautifulSoup
                elif contentStr != SINGLE_LINE_BREAK and contentStr != SPACE_SEPARATOR:
                    # insert line break in the str and convert it to the NavigableString back to the
                    # htmlList[i].contents[0]
                    tagContentsLen = len(tagContents)
                    for j in range(tagContentsLen):
                        if type(tagContents[j]) == NavigableString:
                            if j == tagContentsLen - 1:
                                replacedStr = get_str_after_replacing_single_line_break(
                                    tagContents[j], should_replace_last_char=False
                                )
                                if not (
                                    type(htmlList[i + 1]) == NavigableString
                                    and str(htmlList[i + 1]) == SINGLE_LINE_BREAK
                                        ) \
                                        and tagContents[j][-1] == SINGLE_LINE_BREAK:
                                    shouldLineBreakAddAtTheEnd = True
                            else:
                                replacedStr = get_str_after_replacing_single_line_break(tagContents[j])
                            htmlList[i].contents[j] = NavigableString(replacedStr)
                    tag = htmlList[i]

                    # texts whose color is not the default are in the span tag with class attr
                    classStr = SPACE_SEPARATOR.join(tag.attrs["class"])
                    for key in COLOR_DICT:
                        if classStr == key:
                            htmlList[i].attrs["style"] = COLOR_DICT[key]
        if shouldLineBreakAddAtTheEnd:
            htmlList[i] = str(htmlList[i])+DOUBLE_LINE_BREAK
        else:
            htmlList[i] = str(htmlList[i])

if isPttPost404NotFound:
    sys.exit("article by provided url: 404 not found")

if isImgRenderingEnabled:
    for key in anchorUrlDict.keys():
        if anchorUrlDict[key][1]:
            anchorUrlDict[key][0] = wrap_img_url_or_append_to_dict(anchorUrlDict[key][0])
            imgUrlInMDFormat = anchorUrlDict[key][0]
            # normally, img url will render directly except imgur so only change format when it's from imgur
            if IMGUR_HOST_NAME in imgUrlInMDFormat:
                htmlList[key] = imgUrlInMDFormat

    # render img in span tag now
    for key in spanUrlDict.keys():
        spanUrlDict[key] = wrap_img_url_or_append_to_dict(spanUrlDict[key])
        if ".jpg)" in spanUrlDict[key] \
                or ".png)" in spanUrlDict[key] \
                or ".gif)" in spanUrlDict[key] \
                or ".jpeg)" in spanUrlDict[key]:
            htmlList[key] = spanUrlDict[key]

firstCommentIndex = firstCommentIndex - 2  # 發信站跟文章網址不要 不一定能省略發信站等等(如果發文者馬上編輯的話)
finalList: list = htmlList[3:firstCommentIndex]  # post time and contents
finalList = insert_info_and_get_list(finalList, htmlList, pageUrl)
finalList.append(DOUBLE_LINE_BREAK + "＃")

finalStr = EMPTY_SEPARATOR.join(finalList)
for element in UNEXPECTED_TEXT_LIST:
    finalStr = finalStr.replace(element, EMPTY_SEPARATOR)
finalStr = finalStr.replace(FB_MOBILE_DOMAIN, FB_DESKTOP_DOMAIN)
finalStr = finalStr.replace(THREE_TILDE, THREE_TILDE_REPLACEMENT)

# with open("C:/Users/ASUS/Documents/PythonProject/pttPostCopy/browse.md",
with open(
        "browse.md",
        "w",
        encoding="utf-8"
) as text_file:
    print(finalStr, file=text_file)
