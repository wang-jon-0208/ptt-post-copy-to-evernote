import sys
import warnings
from enum import Enum
from typing import Union, Dict, List
from urllib.parse import unquote

import requests
from bs4 import BeautifulSoup
from bs4.element import NavigableString, Tag
from requests import Response

from globalVarAndMethod import *

warnings.filterwarnings("ignore", ".*looks like a URL.*", category=UserWarning, module='bs4')

if __name__ == '__main__':
    print('PyCharm Pro')


def get_confirmed_18_res(ptt_url_or_shortened_url_but_is_actually_ptt_url):
    response_in_function = requests.get(ptt_url_or_shortened_url_but_is_actually_ptt_url)
    if PTT_18_CONFIRMATION_PAGE in response_in_function.url:
        session_obj = requests.Session()
        payload = {
            "from": PTT_INDEX_PAGE,
            "yes": "yes"
        }
        session_obj.post(PTT_18_CONFIRMATION_PAGE_WITH_FROM_PARAM, payload)
        response_in_function = session_obj.get(ptt_url_or_shortened_url_but_is_actually_ptt_url)

    return response_in_function


def get_html_list(res_in_function: Response) -> list:
    soup = BeautifulSoup(res_in_function.text, features="lxml")
    content_and_comments = soup.select("div.bbs-screen.bbs-content")
    content_and_comments_html = content_and_comments[0]

    return content_and_comments_html.contents


def get_list_except_meta_tag(list_in_function: list) -> list:
    # Besides author, I only want meta value instead of meta tag(作者 看板 標題 時間)
    return list_in_function[1:]


def insert_info_and_get_list(list_in_function: list, html_list: list, page_url) -> list:
    list_in_function.insert(1, html_list[0])  # author
    list_in_function.insert(1, START_ANCHOR + page_url + END_ANCHOR + SINGLE_LINE_BREAK)
    list_in_function.insert(1, html_list[2] + SINGLE_LINE_BREAK)  # title

    return list_in_function


def get_redirected_url(extracted_url: str):
    url_finished = False

    for elementDomain in SHORTENED_URL_NAME_DICT.values():
        if elementDomain in extracted_url:
            try:
                print("redirecting ", extracted_url)
                res_for_shortened_url = requests.get(extracted_url)

                if elementDomain == REURL_CC:
                    extracted_url = res_for_shortened_url.headers["target"]
                else:
                    extracted_url = res_for_shortened_url.url

                res_in_function = get_confirmed_18_res(extracted_url)
                extracted_url = res_in_function.url
                extracted_url = unquote(extracted_url)

                url_finished = True
            except requests.ConnectionError as e:
                print("Umhh... extracted_url=", extracted_url, e)
                # extracted_url = EMPTY_SEPARATOR
                break
    if url_finished:
        print(extracted_url, " redirect ok")
    return extracted_url


def surround_text_with_div(text_in):
    return START_DIV + text_in + END_DIV


def get_yt_info_text(yt_url):
    res_in_method = requests.get(yt_url)
    soup_in_method = BeautifulSoup(res_in_method.text, features="lxml")

    title_tag = soup_in_method.find("meta", attrs={'name': 'title'})
    channel_tag = soup_in_method.find("link", attrs={'itemprop': 'name'})
    date_published_tag = soup_in_method.find("meta", attrs={'itemprop': 'datePublished'})

    if title_tag is None or channel_tag is None or date_published_tag is None:
        text = yt_url
        print("yt url???")
    else:
        title = title_tag.attrs["content"]
        channel = channel_tag.attrs["content"]
        date_published = date_published_tag.attrs["content"]

        text = title + SINGLE_LINE_BREAK \
            + yt_url + SINGLE_LINE_BREAK \
            + channel + SINGLE_LINE_BREAK \
            + date_published

    return text


def get_extracted_url(tag_in_method: Tag):
    if type(tag_in_method.contents[0]) == NavigableString:
        extracted_url: str = str(tag_in_method.contents[0])
    else:  # if it's not NavigableString, then is str
        extracted_url: str = tag_in_method.contents[0]

    redirected_url = get_redirected_url(extracted_url)
    if PTT_18_CONFIRMATION_PAGE in redirected_url:
        redirected_url = PTT_ROOT_URL + redirected_url[35:]
    if redirected_url != EMPTY_SEPARATOR:
        redirected_url = ELIMINATE_REDUNDANT_URL_PARAM_SUFFIX(redirected_url)
        if (YT_DOMAIN in redirected_url
            or YT_SHORTENED_DOMAIN in redirected_url
            or YT_MOBILE_DOMAIN in redirected_url
        ) \
                and "channel" not in redirected_url:
            redirected_url = get_yt_info_text(redirected_url)

    return unquote(redirected_url)


def get_ptt_url_info_without_contents(url_in_function):
    return_str = url_in_function
    is_ptt_post404_not_found = False

    response_in_function = get_confirmed_18_res(url_in_function)
    html_list = get_html_list(response_in_function)

    for i_in_func in range(4):
        item_in_list = html_list[i_in_func]

        if type(item_in_list) == NavigableString:
            html_list[i_in_func] = str(item_in_list)
            string: str = html_list[i_in_func]

            if string == PTT_POST_404_NOT_FOUND:
                is_ptt_post404_not_found = True
                break

        if type(item_in_list) == Tag:
            tag_in_func = item_in_list
            tag_contents: list = tag_in_func.contents

            if tag_in_func.name == "div":
                if len(tag_contents[0].contents) > 0:
                    if str(tag_contents[0].contents[0]) == "作者":
                        space_navigable_str = NavigableString(SPACE_SEPARATOR)
                        html_list[i_in_func].contents.insert(1, space_navigable_str)
                    else:
                        html_list[i_in_func].contents = get_list_except_meta_tag(tag_contents)

        html_list[i_in_func] = str(html_list[i_in_func])

    if is_ptt_post404_not_found:
        return_str = SINGLE_LINE_BREAK + return_str + DOUBLE_LINE_BREAK + "404..."
    else:
        final_list = [html_list[3]]
        final_list = insert_info_and_get_list(final_list, html_list, url_in_function)
        return_str = EMPTY_SEPARATOR.join(final_list)

    return return_str


def is_img_url_with_extension_at_end(url_in_method):
    return url_in_method[-4:] == JPG_SUFFIX or \
           url_in_method[-5:] == JPEG_SUFFIX or \
           url_in_method[-4:] == PNG_SUFFIX or \
           url_in_method[-4:] == GIF_SUFFIX


def get_img_url_at_imgur_with_domain_a(imgur_url):
    res_in_method = requests.get(imgur_url)
    soup_in_method = BeautifulSoup(res_in_method.text, features="lxml")
    big_tag: Tag = soup_in_method.contents[1]
    head_tag: Tag = big_tag.contents[1]
    right_tag: Tag = head_tag.find("meta", attrs={"name": "twitter:image"})
    url: str = right_tag.attrs["content"]
    return url


def wrap_url_in_img_tag(url: str, add_jpg_suffix=True) -> str:
    img_suffix = ""
    if add_jpg_suffix:
        img_suffix = JPG_SUFFIX

    return "<img src=\"" + url + img_suffix + "\" alt=\"\" />" + SINGLE_LINE_BREAK


def generate_img(extracted_url: str) -> str:
    if (extracted_url[:13] == IMGUR_PREFIX or extracted_url[:12] == IMGUR_PREFIX_WITHOUT_S) and \
            not is_img_url_with_extension_at_end(extracted_url):
        if extracted_url[:20] == IMGUR_PREFIX_A:
            extracted_url = get_img_url_at_imgur_with_domain_a(extracted_url)
        extracted_url = wrap_url_in_img_tag(extracted_url)
    # suppose other img urls all have the img extension...
    elif is_img_url_with_extension_at_end(extracted_url):
        extracted_url = wrap_url_in_img_tag(extracted_url, False)
    return extracted_url


def decode_cloud_flare_email(encoded_string):
    r = int(encoded_string[:2], 16)
    email = ''.join(
        [chr(int(encoded_string[i_in_func:i_in_func + 2], 16) ^ r) for i_in_func in range(2, len(encoded_string), 2)]
    )

    return email


class LineBreakMode(Enum):
    Normal = "normal"
    Short = "short"


pageUrl = "https://www.ptt.cc/bbs/nCoV2019/M.1650076406.A.560.html"
isImgRenderingEnabled = True
lineBreakMode = LineBreakMode.Normal
twoHyphenControl = 1  # no signature block: 1, with signature block: 0

response = get_confirmed_18_res(pageUrl)
htmlList = get_html_list(response)
htmlLength = len(htmlList)

emptyNavStr = NavigableString(EMPTY_SEPARATOR)
anchorUrlDict: Dict[int, list] = {}
""" key is htmlList index \n
list index 0 is url, 1 is flag to wrap it 
"""

firstCommentIndex = 0
checkDivCount = 0
commentCount = 0
twoHyphenCount = 1
isPttPost404NotFound = False

allTextList: List[str] = []
for i in range(htmlLength):
    itemInList = htmlList[i]

    if type(itemInList) == NavigableString:
        htmlList[i] = str(itemInList)
        itemStr: str = htmlList[i]

        shouldEndTextAddAtTheEnd = False
        # commentCheckIndex = i + 6  # pass 發信站 文章網址 編輯 and line breaks and url
        # probableCommentStr6 = str(htmlList[commentCheckIndex])
        # probableCommentStr5 = str(htmlList[commentCheckIndex - 1])
        # probableCommentStr4 = str(htmlList[commentCheckIndex - 2])
        # probableCommentStr3 = str(htmlList[commentCheckIndex - 3])
        if (
                # pttArticleEndText in itemStr[-7:] and  # maybe more than 1 line break after --, so, modify the if condition
                pttArticleEndText in itemStr[-7:]  # maybe more than 1 line break after --, so, modify the if condition
                # len(probableCommentStr6) > 20 and
                # (
                #         probableCommentStr6[:18] == "<div class=\"push\">" or
                #         probableCommentStr5[:18] == "<div class=\"push\">" or
                #         probableCommentStr4[:18] == "<div class=\"push\">" or
                #         probableCommentStr3[:18] == "<div class=\"push\">"
                # ) and  # if there are comments
                # commentCheckIndex <= htmlLength - 1
        ):
        # ) or \
        #         (pttArticleEndText in itemStr and commentCheckIndex > htmlLength - 1):  # if no comments
            if twoHyphenCount == twoHyphenControl:
                itemStr = itemStr[:-4]
                shouldEndTextAddAtTheEnd = True
            twoHyphenCount += 1

        if itemStr == PTT_POST_404_NOT_FOUND:
            isPttPost404NotFound = True
            break

        # if lineBreakMode == LineBreakMode.Normal:
        #     if itemStr == SINGLE_LINE_BREAK:
        #         itemStr = HTML_LINE_BREAK + SINGLE_LINE_BREAK
        #     else:
        if itemStr == SINGLE_LINE_BREAK:
            allTextList += itemStr
        else:
            splitList = itemStr.split(SINGLE_LINE_BREAK)
            splitListLen = len(splitList)
            for listI in range(splitListLen):
                # val = splitList[listI]
                if listI != splitListLen - 1:
                    splitList[listI] += SINGLE_LINE_BREAK
                    # if val == EMPTY_SEPARATOR:
                    #     splitList[listI] = HTML_LINE_BREAK + SINGLE_LINE_BREAK
                    # else:
                    #     splitList[listI] = surroundTextWithDiv(val) + SINGLE_LINE_BREAK
                # itemStr = EMPTY_SEPARATOR.join(splitList)
            allTextList += splitList
        # elif lineBreakMode == LineBreakMode.Short:
        #     print("todo")
            # itemStr = itemStr.replace(DOUBLE_LINE_BREAK, SINGLE_LINE_BREAK)
            # itemStr = itemStr.replace(SINGLE_LINE_BREAK, lineBreak + SINGLE_LINE_BREAK)

        if shouldEndTextAddAtTheEnd:
            # itemStr += surroundTextWithDiv("--") + SINGLE_LINE_BREAK + surroundTextWithDiv("#") + SINGLE_LINE_BREAK
            # htmlList[i] = itemStr
            firstCommentIndex = i
            break
        # htmlList[i] = itemStr

    if type(itemInList) == Tag:
        tag = itemInList
        tagContents: list = tag.contents

        shouldAddUrlForAnchorUrlDict = False
        extractedURL = ""
        shouldLineBreakAddAtTheEnd = False
        # if len(tagContents) == 4:
        #     commentCount += 1
        #     if commentCount >= commentNumControl:
        #         firstCommentIndex = i  # todo: delete these 5 lines and use shouldEndTextAddAtTheEnd
        #         break  # we wont address the comments section, so the loop ultimately ends here.

        if tag.name == "div":
            if checkDivCount < 4:  # author, title and post time
                if len(tagContents[0].contents) > 0:
                    if str(tagContents[0].contents[0]) == "作者":
                        spaceNavigableStr = NavigableString(SPACE_SEPARATOR)
                        htmlList[i].contents.insert(1, spaceNavigableStr)
                    else:
                        htmlList[i].contents = get_list_except_meta_tag(tagContents)
                checkDivCount += 1
            if tagContents[0].name == "img":
                if isImgRenderingEnabled:
                    srcAttr = tagContents[0].attrs["src"]
                    if IMGUR_HOST_NAME in srcAttr and MP4_SUFFIX not in srcAttr:
                        # ptt uses cache to show image on their site, but the cache file is smaller, I want the original
                        htmlList[i] = emptyNavStr
                else:
                    htmlList[i] = emptyNavStr
            if tagContents[0].name == "div":
                if len(tagContents[0].contents) == 1 and tagContents[0].contents[0].name == "div":
                    if len(tagContents[0].contents[0].contents) == 1 \
                            and tagContents[0].contents[0].contents[0].name == "iframe":
                        deepTag: Tag = tagContents[0].contents[0].contents[0]
                        if deepTag.attrs["class"][0] == "youtube-player":
                            # if cur element is iframe, previous element in allTextList will always be \n,
                            # I dont want it
                            allTextList[len(allTextList) - 1] = EMPTY_SEPARATOR
            if len(tagContents) >= 4 and type(tagContents[3].contents[0]) == NavigableString:
                ipAndTime = str(tagContents[3].contents[0])
                ipAndTime = ipAndTime.replace(SINGLE_LINE_BREAK, EMPTY_SEPARATOR)
                tagContents[3].contents[0] = NavigableString(ipAndTime)
        if tag.name == "a":
            # todo: address <span>color text<span/> in <a>

            # 最後一行文章網址的URL有多包一層span 不要這個
            if type(tagContents[0]) == str or type(tagContents[0]) == NavigableString:
                tagContentStr = str(tagContents[0])
                if tagContentStr[0] == "#":
                    # make hash code's href from relative url to absolute url
                    tag.attrs["href"] = PTT_ROOT_URL + tag.attrs["href"]

                    href = tag.attrs["href"]
                    htmlList[i] = NavigableString(str(tag) + get_ptt_url_info_without_contents(href))
                elif "class" in tag.attrs and tag.attrs["class"][0] == '__cf_email__':
                    tagContentStr = decode_cloud_flare_email(tag.attrs["data-cfemail"])
                    htmlList[i] = NavigableString(tagContentStr)
                else:
                    # extract the plain URL
                    extractedURL = get_extracted_url(tag)
                    htmlList[i] = extractedURL
                    if extractedURL != EMPTY_SEPARATOR:
                        if PTT_ROOT_URL in extractedURL:
                            htmlList[i] = get_ptt_url_info_without_contents(extractedURL)
                        else:
                            shouldAddUrlForAnchorUrlDict = True

        if tag.name == "span":
            # some url put in span with color and did not change the color in official page
            for tagContentsI in range(len(tagContents)):
                if tagContents[tagContentsI].name == "a":
                    # todo: do logic like if tag.name == "a":
                    anchorTag = tagContents[tagContentsI]
                else:
                    contentStr = str(tagContents[tagContentsI])
                    if contentStr.strip() in PTT_GOSSIPING_POLICY_TEXT:
                        htmlList[i] = emptyNavStr
                    else:
                        if len(tagContents) == 1 and contentStr == SINGLE_LINE_BREAK and type(tag.attrs) == dict:
                            # use clear() to make sure this htmlList[i] will be <span>\n</span>,
                            # in "if tempStr != emptyNavStr",
                            # this will be <span></span> then I can use it to add line break in finalSplitList loop
                            htmlList[i].attrs.clear()
                        # these two can not be wrapped with html and body tags by BeautifulSoup
                        elif contentStr != SINGLE_LINE_BREAK and contentStr != SPACE_SEPARATOR:
                            tag = htmlList[i]

                            # texts whose color is not the default are in the span tag with class attr
                            classStr = SPACE_SEPARATOR.join(tag.attrs["class"])
                            for key in COLOR_DICT:
                                if classStr == key:
                                    htmlList[i].attrs["style"] = COLOR_DICT[key]

        tempStr = htmlList[i]
        if tempStr != emptyNavStr:
            ''' if tempStr is emptyNavStr, it's one of the PTT_GOSSIPING_POLICY_TEXT or the img rendered by ptt,
            I dont want them
            '''
            if shouldAddUrlForAnchorUrlDict:
                anchorUrlDict[len(allTextList) + 1] = [extractedURL, True]

            tempStr = str(tempStr)
            splitList = tempStr.split(SINGLE_LINE_BREAK)
            splitListLenInTag = len(splitList)
            for listI in range(splitListLenInTag):
                if splitList[listI][0:13] == YT_HTTPS_PREFIX or \
                        splitList[listI][0:17] == YT_HTTPS_PREFIX_WITH_WWW or \
                        splitList[listI][0:17] == YT_MOBILE_DOMAIN or \
                        (
                                splitList[listI][0:5] != "<span" and
                                splitList[listI][0:8] != HTTPS_PREFIX and
                                splitList[listI][0:7] != HTTP_PREFIX
                        ):
                    splitList[listI] += SINGLE_LINE_BREAK
            allTextList += splitList

if isPttPost404NotFound:
    sys.exit("article by provided url: 404 not found")

if isImgRenderingEnabled:
    for key in anchorUrlDict.keys():
        if anchorUrlDict[key][1]:
            anchorUrlDict[key][0] = generate_img(anchorUrlDict[key][0])
            imgUrlInTag = SINGLE_LINE_BREAK + anchorUrlDict[key][0]
            # normally, img url will render directly in ptt article except imgur
            # so only change format when it's from imgur
            if IMGUR_HOST_NAME in imgUrlInTag and MP4_SUFFIX not in imgUrlInTag:
                allTextList[key] = imgUrlInTag

joinedAllText = EMPTY_SEPARATOR.join(allTextList)

for element in UNEXPECTED_TEXT_LIST:
    joinedAllText = joinedAllText.replace(element, EMPTY_SEPARATOR)

finalSplitList = joinedAllText.split(SINGLE_LINE_BREAK)
finalSplitListLen = len(finalSplitList)
for listI in range(finalSplitListLen):
    val = finalSplitList[listI]

    finalSplitListLoopContinue = False
    for formatText in GOSSIPING_FORMAT_TEXT_LIST:
        if formatText in val and START_SPAN in val or formatText == val:
            finalSplitList[listI] = EMPTY_SEPARATOR
            finalSplitListLoopContinue = True
            break
    if finalSplitListLoopContinue:
        continue

    if val == EMPTY_SEPARATOR or val == START_SPAN + END_SPAN or val == SPACE_SEPARATOR:
        if lineBreakMode == LineBreakMode.Normal:
            finalSplitList[listI] = HTML_LINE_BREAK + SINGLE_LINE_BREAK
        elif lineBreakMode == LineBreakMode.Short:
            finalSplitList[listI] = EMPTY_SEPARATOR
    else:
        finalSplitList[listI] = surround_text_with_div(val) + SINGLE_LINE_BREAK

startLine = 4
for listI in range(finalSplitListLen):
    if listI >= 4:
        if finalSplitList[listI] != EMPTY_SEPARATOR and finalSplitList[listI] != HTML_LINE_BREAK + SINGLE_LINE_BREAK:
            startLine = listI
            break
finalSplitList = finalSplitList[0:4] + finalSplitList[startLine:]

finalIndex = -1
for index, element in reversed(list(enumerate(finalSplitList))):
    if finalSplitList[index] != HTML_LINE_BREAK + SINGLE_LINE_BREAK:
        finalIndex = index
        break
finalSplitList = finalSplitList[0:finalIndex + 1]

# firstCommentIndex = firstCommentIndex - 2  # 發信站跟文章網址不要 不一定能省略發信站等等(如果發文者馬上編輯的話)
startHtml = START_HTML.replace("title puts here", "PTT_Post")
finalList: list = [startHtml] \
                  + finalSplitList[3:] \
                  + [surround_text_with_div(END_STR)] \
                  + [END_HTML]  # finalSplitList[4] is an unnecessary line break
finalList = insert_info_and_get_list(finalList, finalSplitList, pageUrl)  # post time and contents

finalStr = EMPTY_SEPARATOR.join(finalList)
finalStr = finalStr.replace(FB_MOBILE_DOMAIN, FB_DESKTOP_DOMAIN)

with open(
        "pttPost.html",
        "w",
        encoding="utf-8"
) as text_file:
    print(finalStr, file=text_file)
